FROM php:8.2-fpm


WORKDIR /var/www


RUN apt-get update && apt-get install -y \
      apt-utils \
      libpq-dev \
      libpng-dev \
      libzip-dev \
      zip unzip \
      git && \
      docker-php-ext-install pdo_pgsql && \
      docker-php-ext-install bcmath && \
      docker-php-ext-install gd && \
      docker-php-ext-install zip && \
      apt-get clean


COPY php.ini /usr/local/etc/php/conf.d/php.ini

COPY . /var/www/
COPY package.json   /var/www/package.json
COPY composer.lock  /var/www/composer.lock
COPY composer.json  /var/www/composer.json
COPY artisan   /var/www/artisan


# Install composer
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN curl -sS https://getcomposer.org/installer | php -- \
    --filename=composer \
    --install-dir=/usr/local/bin
# alias
RUN echo "alias a='artisan'" >> /root/.bashrc


RUN composer install \
      --no-interaction \
      --no-plugins \
      --no-suggest \
      --no-scripts \
      --no-autoloader \
      --prefer-dist


RUN groupadd -g 1001 xamex && \
    useradd -u 1001 -ms /bin/bash -g xamex xamex

USER root
RUN composer dump-autoload  --no-scripts --optimize
RUN chown -R $USER:www-data /var/www && \
    chmod 755 -R /var/www && \
    chmod -R 775 /var/www/bootstrap/cache && \
    chmod -R ugo+rw /var/www/storage


    



